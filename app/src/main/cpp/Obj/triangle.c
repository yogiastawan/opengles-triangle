//
// Created by yogiastawan on 4/8/22.
//

#include "triangle.h"
#include <stdlib.h>
#include <assert.h>
#include <android/log.h>
#include "../GlEs/android_gl.h"

#define TAG "Triangle"

static void checkGlError(const char *op);

static GLuint loadShader(GLenum types, const char *shaderSourceCode);

const char *vertexShaderCode = "uniform mat4 uMVPMatrix;"
                               "attribute vec4 vPosition;"
                               "void main(){"
                               "gl_Position=uMVPMatrix*vPosition;"
                               "}";

const char *fragmentShaderCode = "precision mediump float;"
                                 "uniform vec4 vColor;"
                                 "void main(){"
                                 "gl_FragColor=vColor;"
                                 "}";

void triangle_destroy(Triangle *obj) {
    free(obj->vertices);
    free(obj->color);
    free(obj);
}

Triangle *triangle_new() {
    Triangle *obj = malloc(sizeof(Triangle));
    obj->program = glCreateProgram();
    checkGlError("gl create program");
    obj->vertices = (float *) malloc(sizeof(float) * 9);
    //drawing must anticlockwise
    obj->vertices[0] = 0.f;
    obj->vertices[1] = 0.5f;
    obj->vertices[2] = -0.f;
    obj->vertices[3] = -0.5f;
    obj->vertices[4] = -0.5f;
    obj->vertices[5] = 0.0f;
    obj->vertices[6] = 0.5f;
    obj->vertices[7] = -0.5f;
    obj->vertices[8] = 0.0f;
    obj->color = (float *) malloc(sizeof(float) * 4);
    obj->color[0] = 0.0f;
    obj->color[1] = 0.0f;
    obj->color[2] = 1.0f;
    obj->color[3] = 1.0f;
    //create shader
    GLuint vertexShader = loadShader(GL_VERTEX_SHADER, vertexShaderCode);
    GLuint fragShader = loadShader(GL_FRAGMENT_SHADER, fragmentShaderCode);
    glAttachShader(obj->program, vertexShader);
    checkGlError("attach vertex shader");
    checkGlError("attach fragment shader");
    glAttachShader(obj->program, fragShader);
    glLinkProgram(obj->program);
    GLint link_status;
    glGetProgramiv(obj->program, GL_LINK_STATUS, &link_status);
    assert(link_status != 0);
    glDeleteShader(vertexShader);
    glDeleteShader(fragShader);
    return obj;
}

static GLuint loadShader(GLenum type, const char *shaderSourceCode) {
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &shaderSourceCode, NULL);
    glCompileShader(shader);
    GLint compileStatus;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
    assert(compileStatus != 0);
    __android_log_print(ANDROID_LOG_DEBUG, TAG, "Shader: %u => load shader", shader);

    return shader;
}


void triangle_draw(Triangle *obj, mat4x4 model_view_projection_matrix) {
    glUseProgram(obj->program);
    checkGlError("Use program");
    GLint mVPMLocation = glGetUniformLocation(obj->program, "uMVPMatrix");
    glUniformMatrix4fv(mVPMLocation, 1, GL_FALSE, (const GLfloat *) model_view_projection_matrix);
    GLint positionAttribute = glGetAttribLocation(obj->program, "vPosition");
    glEnableVertexAttribArray(positionAttribute);
    checkGlError("Enable attrib");
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, 0,
                          obj->vertices);
    checkGlError("vertex attrib");
    GLint colorUniform = glGetUniformLocation(obj->program, "vColor");
    glUniform4fv(colorUniform, 1, obj->color);
    glDrawArrays(GL_TRIANGLES, 0, 3); // obj.vertices.length/3
    checkGlError("Draw array");
    glDisableVertexAttribArray(positionAttribute);
}

static void checkGlError(const char *op) {
    for (GLuint error = glGetError(); error; error = glGetError()) {
        __android_log_print(ANDROID_LOG_INFO, TAG, "after %s() glError (0x%x)\n", op, error);
    }
}

