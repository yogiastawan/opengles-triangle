//
// Created by yogiastawan on 4/8/22.
//

#ifndef OGLES_TRIANGLE_TRIANGLE_H
#define OGLES_TRIANGLE_TRIANGLE_H

#include "../linmath/linmath.h"

#ifdef __cplusplus
extern c{
#endif

typedef struct _triangle Triangle;

struct _triangle {
    float *vertices;
    float *color;
    unsigned int program;
};

Triangle *triangle_new();

void triangle_draw(Triangle *obj,mat4x4 model_view_projection_matrix);
//void triangle_draw(Triangle *obj);

void triangle_destroy(Triangle *obj);


#ifdef __cplusplus
};
#endif

#endif //OGLES_TRIANGLE_TRIANGLE_H
