#include <jni.h>
#include <string>

#include "render_wrapper.h"

extern "C"
JNIEXPORT void JNICALL
Java_com_ether_oglestriangle_RenderWrapper_surfaceCreated(JNIEnv *env, jobject thiz) {
    on_surface_created();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_ether_oglestriangle_RenderWrapper_surfaceChanged(JNIEnv *env, jobject thiz, jint width,
                                                          jint height) {
    on_surface_changed(width, height);
}
extern "C"
JNIEXPORT void JNICALL
Java_com_ether_oglestriangle_RenderWrapper_drawFrame(JNIEnv *env, jobject thiz) {
    on_draw_frame();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_ether_oglestriangle_RenderWrapper_destroy(JNIEnv *env, jobject thiz) {
    destroy();
}