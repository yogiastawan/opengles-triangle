//
// Created by yogiastawan on 4/9/22.
//

#include "render_wrapper.h"
#include "GlEs/android_gl.h"
#include "Obj/triangle.h"
#include <android/log.h>

#include "linmath/linmath.h"
#include <jni.h>

#define LOG_TAG "RenderWrapperC"

static inline float deg_to_radf(float deg);

static void position_object_in_scene(float x, float y, float z);

static mat4x4 projection_matrix;
static mat4x4 model_matrix;
static mat4x4 view_matrix;


static mat4x4 view_projection_matrix;
static mat4x4 model_view_projection_matrix;
static mat4x4 inverted_view_projection_matrix;

static Triangle *obj1;
static float angle;

mat4x4 befrot;

void on_surface_created() {
    glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    obj1 = triangle_new();
    mat4x4_identity(befrot);
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "Program: %u => surface_created",
                        obj1->program);
}

void on_surface_changed(int width, int height) {
    glViewport(0, 0, width, height);
    mat4x4_perspective(projection_matrix, deg_to_radf(45), (float) width / (float) height, 1.0f,
                       10.0f);
    mat4x4_look_at(view_matrix, (vec3) {0.0f, 1.2f, 2.2f}, (vec3) {0.0f, 0.0f, 0.0f},
                   (vec3) {0.0f, 1.0f, 0.0f});
}

void on_draw_frame() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    mat4x4_mul(view_projection_matrix, projection_matrix, view_matrix);
    position_object_in_scene(0, 0, 0);
    mat4x4 rotationMatrix;
    mat4x4_rotate(rotationMatrix, befrot, 0.f, 0.f, -1.f, deg_to_radf(angle));
    mat4x4 res;
    mat4x4_mul(res,model_view_projection_matrix,rotationMatrix);
    triangle_draw(obj1, res);
//    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "Program: %u => draw_frame || %f", obj1->program, obj1->vertices[3]);
}

void destroy() {
    triangle_destroy(obj1);
}

static inline float deg_to_radf(float deg) {
    return deg * (float) M_PI / 180.0f;
}

static void position_object_in_scene(float x, float y, float z) {
    mat4x4_identity(model_matrix);
    mat4x4_translate_in_place(model_matrix, x, y, z);
    mat4x4_mul(model_view_projection_matrix, view_projection_matrix, model_matrix);
}


JNIEXPORT void JNICALL
Java_com_ether_oglestriangle_RenderWrapper_setRotate(JNIEnv *env, jobject thiz, jfloat value) {
    angle = value;
}