//
// Created by yogiastawan on 4/9/22.
//

#ifndef OGLES_TRIANGLE_RENDER_WRAPPER_H
#define OGLES_TRIANGLE_RENDER_WRAPPER_H
#ifdef __cplusplus
extern "C" {
#endif

void on_surface_created();

void on_surface_changed(int width, int height);

void on_draw_frame();

void destroy();

#ifdef __cplusplus
}
#endif
#endif //OGLES_TRIANGLE_RENDER_WRAPPER_H
