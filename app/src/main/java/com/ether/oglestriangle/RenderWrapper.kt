package com.ether.oglestriangle

import android.opengl.GLSurfaceView
import android.util.Log
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class RenderWrapper : GLSurfaceView.Renderer {
    override fun onSurfaceCreated(p0: GL10?, p1: EGLConfig?) {
//        Log.d("RenderWrapperKotlin","onSurfaceCreated")
        surfaceCreated()
    }

    override fun onSurfaceChanged(p0: GL10?, p1: Int, p2: Int) {
//        Log.d("RenderWrapperKotlin","onSurfaceChanged")
        surfaceChanged(p1, p2)
    }

    override fun onDrawFrame(p0: GL10?) {
        drawFrame()
    }

    private external fun surfaceCreated()
    private external fun surfaceChanged(width: Int, height: Int)
    private external fun drawFrame()
    external fun setRotate(value:Float)
    external fun destroy()

    companion object {
        init {
            System.loadLibrary("oglestriangle")
        }
    }
}